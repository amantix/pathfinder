﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityQueue
{
    public interface IQueue<TItem>
    {
        void Enqueue(TItem item);
        TItem Dequeue();
        TItem Peek();
    }

    public class PriorityQueue<TItem, TPriority> : IQueue<TItem>
        where TPriority : IComparable<TPriority>
    {
        public Func<TItem, TPriority> PriorityStrategy { get; }
        public int Count => _heap.Count;

        private BinaryHeap<TItem, TPriority> _heap;

        public PriorityQueue(Func<TItem, TPriority> priorityStrategy)
        {
            PriorityStrategy = priorityStrategy;
            _heap = new BinaryHeap<TItem, TPriority>(priorityStrategy);
        }

        public void Enqueue(TItem item)
        {
            _heap.Add(item);
        }

        public TItem Dequeue()
        {
            return _heap.Remove();
        }

        public TItem Peek()
        {
            return _heap.Peek();
        }
    }

    public class BinaryHeap<TItem, TPriority>
        where TPriority : IComparable<TPriority>
    {
        private Func<TItem, TPriority> _priorityStrategy;
        private List<TItem> _items = new List<TItem>();

        public BinaryHeap(Func<TItem, TPriority> priorityStrategy)
        {
            _priorityStrategy = priorityStrategy;
        }

        public int Count => _items.Count;

        public TItem Peek()
        {
            if (_items.Count > 0)
                return _items[0];
            throw new InvalidOperationException("Heap is empty");
        }

        public TItem Remove()
        {
            if (_items.Count == 0)
                throw new InvalidOperationException("Heap is empty");

            var temp = _items[0];
            _items[0] = _items[_items.Count - 1];
            _items.RemoveAt(_items.Count - 1);
            SiftDown();
            return temp;
        }

        private void SiftDown()
        {
            int i = 0;
            while (2 * i + 1 < _items.Count)
            {
                int j = 2 * i + 1;
                if (j + 1 < _items.Count &&
                    _priorityStrategy(_items[j + 1])
                    .CompareTo(_priorityStrategy(_items[j])) > 0)
                {
                    j++;
                }
                if (_priorityStrategy(_items[j])
                    .CompareTo(_priorityStrategy(_items[i])) > 0)
                {
                    var temp = _items[i];
                    _items[i] = _items[j];
                    _items[j] = temp;
                }
                else break;
                i = j;
            }
        }

        public void Add(TItem item)
        {
            _items.Add(item);
            SiftUp();
        }

        private void SiftUp()
        {
            for (int i = _items.Count - 1; i > 0; i = (i - 1) / 2)
            {
                if (_priorityStrategy(_items[i])
                    .CompareTo(_priorityStrategy(_items[(i - 1) / 2])) > 0)
                {
                    var temp = _items[i];
                    _items[i] = _items[(i - 1) / 2];
                    _items[(i - 1) / 2] = temp;
                }
                else break;
            }
        }
    }
}
