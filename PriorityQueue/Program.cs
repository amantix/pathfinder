﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PriorityQueue
{

    public enum Direction
    {
        Right, Down, Left, Up
    }

    public abstract class PathFinder
    {
        protected PathFinder(string filename)
        {
            using (var sr = new StreamReader("input.txt"))
            {
                var tmp = Array.ConvertAll(sr.ReadLine().Split(' '), Convert.ToInt32);

                _matrix = new int[tmp[0], tmp[1]];

                for (int i = 0; i < tmp[0]; i++)
                {
                    var str = sr.ReadLine();
                    for (int j = 0; j < tmp[1]; j++)
                    {
                        int value;
                        switch (str[j])
                        {
                            case 't':
                                value = int.MaxValue;
                                break;
                            case 's':
                                value = int.MinValue;
                                break;
                            case '#':
                                value = -1;
                                break;
                            default:
                                value = 0;
                                break;
                        }
                        _matrix[i, j] = value;
                    }
                }
            }
        }
        public IEnumerable<Direction> FindPath()
        {
            _counter = 1;

            var result = FindPathImpl(new List<Direction>(), 1, 1);

            using (var sw = new StreamWriter("output.txt"))
            {
                for (int i = 0; i < _matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < _matrix.GetLength(1); j++)
                        sw.Write(_matrix[i, j] > 0 ? '#' : ' ');
                    sw.WriteLine();
                }
            }
            return result;
        }

        protected int[,] _matrix;
        protected int _counter;
        protected bool IsValidStep(int toX, int toY)
        {
            return toX >= 0 && toX < _matrix.GetLength(0) &&
                toY >= 0 && toY < _matrix.GetLength(1) &&
                (_matrix[toX, toY] == 0 || _matrix[toX, toY] == int.MaxValue);
        }
        protected bool IsTarget(int x,int y)
        {
            return x >= 0 && x < _matrix.GetLength(0) &&
                   y >= 0 && y < _matrix.GetLength(1) && 
                   _matrix[x, y] == int.MaxValue;
        }

        protected abstract List<Direction> FindPathImpl(List<Direction> path, int x, int y);

    }

    public class RecursivePathFinder : PathFinder
    {
        public RecursivePathFinder(string filename) : base(filename)
        {           
        }
        
        protected override List<Direction> FindPathImpl(List<Direction> path,int x, int y)
        {
            if (IsTarget(x,y)) return path;
            _matrix[x, y] = _counter++;

            if (IsValidStep(x + 1, y))
            {
                var l = new List<Direction>(path) {Direction.Down};
                l = FindPathImpl(l, x + 1, y);
                if (l.Count > 0) return l;
            }

            if (IsValidStep(x, y - 1))
            {
                var l = new List<Direction>(path) {Direction.Left};
                l = FindPathImpl(l, x, y - 1);
                if (l.Count > 0) return l;
            }

            if (IsValidStep(x,y+1))
            {
                var l = new List<Direction>(path) {Direction.Right};
                l= FindPathImpl(l,x, y + 1);
                if (l.Count > 0) return l;
            }
                  
            if (IsValidStep(x-1, y))
            {
                var l = new List<Direction>(path) {Direction.Up};
                l= FindPathImpl(l, x-1, y);
                if (l.Count > 0) return l;
            }
            return new List<Direction>();
        }
    }


    public class StackPathFinder : PathFinder
    {
        public StackPathFinder(string filename): base(filename)
        { 
        }
        
        protected override List<Direction> FindPathImpl(List<Direction> path, int x, int y)
        {
            Stack<Tuple<List<Direction>, int, int>> stack =
                new Stack<Tuple<List<Direction>, int, int>>();
            stack.Push(new Tuple<List<Direction>, int, int>(path, x, y));

            while (stack.Count > 0)
            {
                var tmp = stack.Pop();

                if (IsTarget(tmp.Item2,tmp.Item3))
                    return tmp.Item1;
                _matrix[tmp.Item2, tmp.Item3] = _counter++;

                if (IsValidStep(tmp.Item2 - 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) {Direction.Up};
                    stack.Push(new Tuple<List<Direction>, int, int>(l, tmp.Item2 - 1, tmp.Item3));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 + 1))
                {
                    var l = new List<Direction>(tmp.Item1) {Direction.Right};
                    stack.Push(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 + 1));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 - 1))
                {
                    var l = new List<Direction>(tmp.Item1) {Direction.Left};
                    stack.Push(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 - 1));
                }
                if (IsValidStep(tmp.Item2 + 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) {Direction.Down};
                    stack.Push(new Tuple<List<Direction>, int, int>(l, tmp.Item2 + 1, tmp.Item3));  
                }
            }
            return new List<Direction>();
        }
    }

    public class QueuePathFinder : PathFinder
    {
        public QueuePathFinder(string filename) : base(filename)
        {
        }
        
        protected override List<Direction> FindPathImpl(List<Direction> path, int x, int y)
        {
            Queue<Tuple<List<Direction>, int, int>> queue =
                new Queue<Tuple<List<Direction>, int, int>>();
            queue.Enqueue(new Tuple<List<Direction>, int, int>(path, x, y));

            while (queue.Count > 0)
            {
                var tmp = queue.Dequeue();

                if (IsTarget(tmp.Item2, tmp.Item3))
                    return tmp.Item1;
                _matrix[tmp.Item2, tmp.Item3] = _counter++;

                if (IsValidStep(tmp.Item2 - 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Up };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2 - 1, tmp.Item3));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 + 1))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Right };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 + 1));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 - 1))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Left };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 - 1));
                }
                if (IsValidStep(tmp.Item2 + 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Down };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2 + 1, tmp.Item3));
                }
            }
            return new List<Direction>();
        }
    }


    public class PriorityQueuePathFinder : PathFinder
    {
        public PriorityQueuePathFinder(string filename) : base(filename)
        { }
        
        
        protected override List<Direction> FindPathImpl(List<Direction> path, int x, int y)
        {
            PriorityQueue<Tuple<List<Direction>, int, int>,int> queue =
                new PriorityQueue<Tuple<List<Direction>, int, int>,int>
                (q=>-(int)Math.Sqrt((q.Item2-101)*(q.Item2-101) + (q.Item3-51)*(q.Item3-51))-Math.Abs(q.Item3-51));
            queue.Enqueue(new Tuple<List<Direction>, int, int>(path, x, y));

            while (queue.Count > 0)
            {
                var tmp = queue.Dequeue();

                if (IsTarget(tmp.Item2, tmp.Item3))
                    return tmp.Item1;
                _matrix[tmp.Item2, tmp.Item3] = _counter++;

                if (IsValidStep(tmp.Item2 - 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Up };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2 - 1, tmp.Item3));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 + 1))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Right };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 + 1));
                }
                if (IsValidStep(tmp.Item2, tmp.Item3 - 1))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Left };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2, tmp.Item3 - 1));
                }
                if (IsValidStep(tmp.Item2 + 1, tmp.Item3))
                {
                    var l = new List<Direction>(tmp.Item1) { Direction.Down };
                    queue.Enqueue(new Tuple<List<Direction>, int, int>(l, tmp.Item2 + 1, tmp.Item3));
                }
            }
            return new List<Direction>();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //var result = new RecursivePathFinder("input.txt").FindPath();
            //var result = new StackPathFinder("input.txt").FindPath();
            //var result = new QueuePathFinder("input.txt").FindPath();
            var result = new PriorityQueuePathFinder("input.txt").FindPath();

            foreach (var dir in result)
                Console.WriteLine(dir);
        }
    }
}
